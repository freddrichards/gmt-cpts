#!/bin/bash
# 
# Script to plot geological time scale cpts
# 
# Christian Heine, mailto:christian.heine@sydney.edu.au
# 2013-10-25
# 
# Requires: GMT5 [psscale,ps2raster]


baseName="GTS2012"
cptPath="${baseName}/${baseName}"

for i in eons eras epochs ages; do

psfile="$baseName/${baseName}_${i}.ps"
    
echo "Plotting $i"
    
gmt psscale -D0/12.5/-25/1 -A -P -C${cptPath}_${i}.cpt -Ef -K  >  $psfile
gmt psscale -D2/12.5/-25/1 -C${cptPath}_${i}.cpt -L0.1 -Ef -O -K >>  $psfile
gmt pstext -R0/5/0/30 -JX5/30 -O -F+f12p,Helvetica,red+jBC <<END >> $psfile
1 26 $baseName
END
    
#-- Convert to PNG
gmt ps2raster $psfile -Tg -A1c

rm -f $psfile
    
done

#--- Plot mag ano timescale

baseName="GeeK07"
cptPath="MagAnomaly/${baseName}"

gmt psscale -D0/12.5/-100/1 -A -P -C${cptPath}.cpt -Ef -K  >  MagAnomaly/${baseName}.ps
gmt psscale -D2/12.5/-100/1 -C${cptPath}.cpt -L0.1 -Ef -O >>  MagAnomaly/${baseName}.ps
#-- Convert to PNG
gmt ps2raster MagAnomaly/${baseName}.ps -Tg -A1c

rm -f MagAnomaly/${baseName}.ps


